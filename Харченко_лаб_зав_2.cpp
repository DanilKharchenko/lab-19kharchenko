#include <graphics.h>
#include <conio.h>      
#include <math.h>     
int main(void) 
{
float x, y;
int gdriver = DETECT, gmode;
initgraph(&gdriver, &gmode, ""); 
line(50,0,50,480); 
line(50,200,640,200);
 outtextxy(60,210,"0");
 outtextxy(610,210,"X");
outtextxy(55,10,"Y"); 
line(50, 0, 60,20);
line(50, 0, 40,20);
line(615,210,640,200);
line(615,190,640,200);
line(40,30,65,30);
line(40,50,65,50);
line(40,70,65,70);
line(40,90,65,90);
line(40,110,65,110);
line(40,130,65,130);
line(40,150,65,150);
line(40,170,65,170);
line(40,190,65,190);
line(40,210,65,210);
line(40,230,65,230);
line(40,250,65,250);
outtextxy(40,190,"1");
outtextxy(40,170,"2");
outtextxy(40,150,"3");
outtextxy(40,130,"4");
outtextxy(40,110,"5");
outtextxy(40,90,"6");
outtextxy(40,70,"7");
line(70,210,70,185);
outtextxy(70,210,"1");
outtextxy(90,210,"2");
outtextxy(110,210,"3");
outtextxy(130,210,"4");
outtextxy(150,210,"5");
outtextxy(170,210,"6");
line(90,210,90,185);
line(110,210,110,185);
line(130,210,130,185);
line(150,210,150,185);
line(170,210,170,185);
line(510,150,510,20);
line(410,150,410,20);
line(460,150,460,20);
line(410,150,510,150);
line(410,20,510,20);
outtextxy(430,4,"x");
outtextxy(480,4,"y");
outtextxy(430,25,"1"); outtextxy(480,25,"1");
outtextxy(430,45,"2"); outtextxy(480,45,"1,5");
outtextxy(430,65,"3"); outtextxy(480,65,"1,6");
outtextxy(430,85,"4"); outtextxy(480,85,"2");
outtextxy(430,105,"4"); outtextxy(480,105,"5");
outtextxy(430,125,"7"); outtextxy(480,125,"5,2");
moveto(50,200);                            
x=0;
do
{
setcolor(3);
y=1/sqrt(2-x);
lineto(50+x*50, 100+(100-(y*20)));     
x=x+0.02;
} while(x<10);
getch();                 
closegraph();        
return 0;
}
